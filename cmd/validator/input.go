package validator

import (
	"regexp"
)

type InputValidator struct {
}

func NewInputValidator() *InputValidator {
	return &InputValidator{}
}

func (i *InputValidator) IsValidInput(inp string) bool {
	v, err := regexp.Match(`(?mi)^[A-H][1-8]$`, []byte(inp))
	if err != nil {
		return false
	}

	return v
}
