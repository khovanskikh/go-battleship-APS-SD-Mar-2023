package main

import (
	"bufio"
	"fmt"
	"go-battleship/cmd/validator"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"go-battleship/cmd/console"
	"go-battleship/cmd/controller"
	"go-battleship/cmd/letter"
)

var (
	enemyFleet []*controller.Ship
	myFleet    []*controller.Ship
	scanner    *bufio.Scanner
)
var printer = console.ColoredPrinter(
	1,
	false,
).Background(console.BLACK).Foreground(console.YELLOW).Build()
var inputValidator = validator.NewInputValidator()

// []

func main() {
	rand.Seed(time.Now().UnixNano())

	scanner = bufio.NewScanner(os.Stdin)

	printer.SetForegroundColor(console.MAGENTA)
	printer.Println("                                     |__")
	printer.Println("                                     |\\/")
	printer.Println("                                     ---")
	printer.Println("                                     / | [")
	printer.Println("                              !      | |||")
	printer.Println("                            _/|     _/|-++'")
	printer.Println("                        +  +--|    |--|--|_ |-")
	printer.Println("                     { /|__|  |/\\__|  |--- |||__/")
	printer.Println("                    +---------------___[}-_===_.'____                 /\\")
	printer.Println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _")
	printer.Println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7")
	printer.Println("|                        Welcome to Battleship                         BB-61/")
	printer.Println(" \\_________________________________________________________________________|")
	printer.Println("")
	printer.ResetForegroundColor()

	initializeGame()

	startGame()
}

func startGame() {
	printer.Print("\033[2J\033[;H")
	printer.Println("                  __")
	printer.Println("                 /  \\")
	printer.Println("           .-.  |    |")
	printer.Println("   *    _.-'  \\  \\__/")
	printer.Println("    \\.-'       \\")
	printer.Println("   /          _/")
	printer.Println("  |      _  /\" \"")
	printer.Println("  |     /_\\'")
	printer.Println("   \\    \\_/")
	printer.Println("    \" \"\" \"\" \"\" \"")

	for {
		printer.Println("")
		printer.Println("Player, it's your turn")
		printer.Println("Enter coordinates for your shot :")
		var isHit bool
		for scanner.Scan() {
			input := scanner.Text()
			if input != "" {
				position := parsePosition(input)
				var err error
				isHit, err = controller.CheckIsHit(enemyFleet, position)
				if err != nil {
					printer.SetForegroundColor(console.RED)
					printer.Printf("Error: %s\n", err)
					printer.ResetForegroundColor()
				}
				break
			}
		}

		if isHit {
			beep()
			printer.SetForegroundColor(console.RED)
			printExplode()
			printer.ResetForegroundColor()
		}

		if isHit {
			printer.SetForegroundColor(console.RED)
			printer.Println("Yeah ! Nice hit !")
			printer.ResetForegroundColor()
		} else {
			printer.SetForegroundColor(console.CADET_BLUE)
			printer.Println("Miss")
			printer.ResetForegroundColor()
		}

		position := getRandomPosition()
		var err error
		isHit, err = controller.CheckIsHit(myFleet, position)
		if err != nil {
			printer.Printf("Error: %s\n", err)
		}
		printer.Println("")

		if isHit {
			printer.SetForegroundColor(console.RED)

			printer.Printf("Computer shoot in %s%d and miss\n", position.Column, position.Row)
			beep()
			printExplode()

			printer.ResetForegroundColor()
		} else {
			printer.SetForegroundColor(console.CADET_BLUE)
			printer.Printf("Computer shoot in %s%d and hit your ship!\n", position.Column, position.Row)
			printer.ResetForegroundColor()
		}

		printer.SetForegroundColor(console.MAGENTA)
		printer.Println("")
		printer.Println("////////////////////////////////////////////")
		printer.Println("/////////////// next round /////////////////")
		printer.Println("////////////////////////////////////////////")
		printer.ResetForegroundColor()
	}
}

func printExplode() {
	printer.SetForegroundColor(console.RED)
	printer.Println("                \\         .  ./")
	printer.Println("              \\      .:\" \";'.:..\" \"   /")
	printer.Println("                  (M^^.^~~:.'\" \").")
	printer.Println("            -   (/  .    . . \\ \\)  -")
	printer.Println("               ((| :. ~ ^  :. .|))")
	printer.Println("            -   (\\- |  \\ /  |  /)  -")
	printer.Println("                 -\\  \\     /  /-")
	printer.Println("                   \\  \\   /  /")
	printer.ResetForegroundColor()
}

func parsePosition(input string) *controller.Position {
	ltr := strings.ToUpper(string(input[0]))
	number, _ := strconv.Atoi(string(input[1]))
	return &controller.Position{
		Column: letter.FromString(ltr),
		Row:    number,
	}
}

func beep() {
	fmt.Print("\007")
}

func initializeGame() {
	switch rand.Intn(5) + 1 {
	case 1:
		initializeEnemyFleet1()
	case 2:
		initializeEnemyFleet2()
	case 3:
		initializeEnemyFleet3()
	case 4:
		initializeEnemyFleet4()
	case 5:
		initializeEnemyFleet5()
	}

	initializeMyFleet()
}

func initializeMyFleet() {
	//reader := bufio.NewReader(os.Stdin)
	//scanner := bufio.NewScanner(os.Stdin)
	myFleet = controller.InitializeShips()

	printer.Println("Please position your fleet (Game board has size from A to H and 1 to 8) :")

	for _, ship := range myFleet {
		printer.Println("")
		printer.Printf("Please enter the positions for the %s (size: %d)", ship.Name, ship.Size)
		printer.Println("")

		for i := 1; i <= ship.Size; i++ {
			printer.Printf("Enter position %d of %d (i.e A3):\n", i, ship.Size)

			for scanner.Scan() {
				positionInput := scanner.Text()
				if positionInput != "" {
					if !inputValidator.IsValidInput(positionInput) {
						printer.Println("Invalid format. Please try again.")
						i--
						break
					}
					ship.AddPosition(positionInput)
					break
				}
			}
		}
	}
}

func getRandomPosition() *controller.Position {
	rows := 8
	lines := 8
	letter := letter.Letter(rand.Intn(lines-1) + 1)
	number := rand.Intn(rows-1) + 1
	return &controller.Position{Column: letter, Row: number}
}

func initializeEnemyFleet1() {
	writeLog("Enemy fleet 1")

	enemyFleet = controller.InitializeShips()

	enemyFleet[0].SetPositions(&controller.Position{Column: letter.G, Row: 1})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.G, Row: 2})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.G, Row: 3})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.G, Row: 4})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.G, Row: 5})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 6})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 6})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.C, Row: 6})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.D, Row: 6})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.A, Row: 1})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.B, Row: 1})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.C, Row: 1})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.A, Row: 4})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.B, Row: 4})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.C, Row: 4})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.G, Row: 7})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.H, Row: 7})
}

func initializeEnemyFleet2() {
	writeLog("Enemy fleet 2")

	enemyFleet = controller.InitializeShips()

	enemyFleet[0].SetPositions(&controller.Position{Column: letter.D, Row: 3})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.E, Row: 3})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.F, Row: 3})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.G, Row: 3})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 3})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 3})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 4})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 5})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 6})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.D, Row: 6})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.D, Row: 7})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.D, Row: 8})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 5})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 6})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.H, Row: 7})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.B, Row: 8})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.C, Row: 8})
}

func initializeEnemyFleet3() {
	writeLog("Enemy fleet 3")

	enemyFleet = controller.InitializeShips()

	enemyFleet[0].SetPositions(&controller.Position{Column: letter.C, Row: 1})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.D, Row: 1})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.E, Row: 1})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.F, Row: 1})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.G, Row: 1})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 3})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 4})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 5})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 6})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.F, Row: 3})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.F, Row: 4})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.F, Row: 5})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.E, Row: 8})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.F, Row: 8})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.G, Row: 8})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.B, Row: 8})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.C, Row: 8})
}

func initializeEnemyFleet4() {
	writeLog("Enemy fleet 4")

	enemyFleet = controller.InitializeShips()

	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 4})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 5})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 6})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 7})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.H, Row: 8})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.A, Row: 2})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 2})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.C, Row: 2})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.D, Row: 2})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.F, Row: 1})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.G, Row: 1})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.H, Row: 1})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.B, Row: 4})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.C, Row: 4})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.E, Row: 4})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.E, Row: 7})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.F, Row: 7})
}

func initializeEnemyFleet5() {
	writeLog("Enemy fleet 5")

	enemyFleet = controller.InitializeShips()

	enemyFleet[0].SetPositions(&controller.Position{Column: letter.D, Row: 2})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.D, Row: 3})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.D, Row: 4})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.D, Row: 5})
	enemyFleet[0].SetPositions(&controller.Position{Column: letter.D, Row: 6})

	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 1})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 2})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 3})
	enemyFleet[1].SetPositions(&controller.Position{Column: letter.B, Row: 4})

	enemyFleet[2].SetPositions(&controller.Position{Column: letter.A, Row: 6})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.A, Row: 7})
	enemyFleet[2].SetPositions(&controller.Position{Column: letter.A, Row: 8})

	enemyFleet[3].SetPositions(&controller.Position{Column: letter.F, Row: 1})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.F, Row: 2})
	enemyFleet[3].SetPositions(&controller.Position{Column: letter.F, Row: 3})

	enemyFleet[4].SetPositions(&controller.Position{Column: letter.G, Row: 6})
	enemyFleet[4].SetPositions(&controller.Position{Column: letter.G, Row: 7})
}
